(function() {
    'use strict';
    angular
        .module('app.core')
        .factory('HttpInterceptorErrores', HttpInterceptorErrores );



HttpInterceptorErrores.$inject = ['$q','$rootScope','$log','$injector'];
 function  HttpInterceptorErrores ($q, $rootScope, $log,$injector) {
    //var $mdToast = $injector.get('$mdToast');
    return {
        request: function (config) {
            return config || $q.when(config)
        },
        response: function (response) {
            return response || $q.when(response);
        },
        responseError: function (response) {
            if (response.status === 401) {
                //here I preserve login page 
                console.log("Intercepte la respuesta en error");

                //$mdToast.simple("No autenticado");
                $rootScope.$broadcast('error')
            }
            return $q.reject(response);
        }
    };
};

})();
