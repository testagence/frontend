(function() {
    'use strict';
    angular.module( 'app.core', 
                    ['ngMaterial',
                    'ui.router',
                    'pascalprecht.translate',
                    'ngMessages',
                    'md.data.table',
                    'chart.js'] );

})();


