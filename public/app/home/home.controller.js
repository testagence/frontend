(function () {
    'use strict';
    angular
        .module('app.home', ['app.core']);
    angular
        .module('app.home')
        .controller('HomeController', HomeController);

    HomeController.$inject = ['$mdDialog', '$filter', '$mdToast', 'ConsultoresService', 'GananaciaService', '$state']
    function HomeController($mdDialog, $filter, $mdToast, ConsultoresService, GananaciaService, $state) {


        var vm = this;
        var $translate = $filter('translate');

        vm.cargandoInforme = false;

        vm.addCheck = addCheck;
        vm.cargarDatosIniciales = cargarDatosIniciales;
        vm.fromYear = "2003";
        vm.fromMonth = "01";
        vm.toYear = "2003";
        vm.toMonth = "01";
        vm.generateObjeto = generateObjeto;
        vm.listaYears = ["2003", "2004", "2005", "2006", "2007"];
        vm.listaMeses = ["01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12"];
        vm.listaConsultores = [];
        vm.listaGanancias = [];
        vm.prepararListaConsultores = prepararListaConsultores;
        vm.removeCheck = removeCheck;

        vm.consultarInformeGanancias = consultarInformeGanancias;
        vm.generarGraficoPie = generarGraficoPie;
        vm.generarGrafico=generarGrafico;
        vm.generarInformeGanancias = generarInformeGanancias;

        vm.mostrarChart = mostrarChart;
        vm.mostrarPieChart = mostrarPieChart;
        vm.mostrarTable = mostrarTable;

        vm.visiblePieChart = false;
        vm.visibleTable = false;
        vm.visibleChart = false;

        vm.dataPie = [];
        vm.labelsPie = [];
        vm.optionsPie = {
            legend: {
                display: true,
                position: 'right',

            }
        };

        vm.dataChart = [];
        vm.labelsChart = [];
        vm.optionsChart = {
            legend: {
                display: true,
                position: 'right',

            }
        };

        vm.datasetOverrideChart = [
            {
                label: $translate('CHART_GANANCIAS'),
                borderWidth: 1,
                type: 'bar'
            },
            {
                label: $translate('CHART_COSTOS'),
                borderWidth: 3,
                hoverBackgroundColor: "rgba(255,99,132,0.4)",
                hoverBorderColor: "rgba(255,99,132,1)",
                type: 'line'
            }
        ];

        vm.chartColors = ['#607D8B', '#009688', '#4527A0','#2E7D32','#757575','#37474F'];

        vm.cargarDatosIniciales();

        function mostrarPieChart() {
            vm.visiblePieChart = true;
            vm.visibleTable = false;
            vm.visibleChart = false;

        }

        function mostrarTable() {
            vm.visiblePieChart = false;
            vm.visibleTable = true;
            vm.visibleChart = false;

        }

        function mostrarChart() {
            vm.visiblePieChart = false;
            vm.visibleTable = false;
            vm.visibleChart = true;

        }


        function cargarDatosIniciales() {
            ConsultoresService.loadListaConsultores()
                .then(loadListaConsultoresDone)
                .catch(loadListaConsultoresError)

            function loadListaConsultoresDone(response) {
                if (response.status == true) {
                    vm.listaConsultores = [];
                    for (var i = 0; i < response.result.length; i++) {
                        var objeto = response.result[i];
                        vm.listaConsultores.push(vm.generateObjeto(objeto));
                    }
                }
            }

            function loadListaConsultoresError(error) {
                console.log("Error carga de datos en controlador");
            }
        }

        function generarGrafico() {
            vm.dataChart = [];
            vm.labelsChart = [];
            vm.mostrarChart();
            vm.consultarInformeGanancias()
                .then(consultarInformeGananciasDone)
                .catch(consultarInformeGananciasError);

            function consultarInformeGananciasDone(results) {
                var arregloA = [];
                var arregloB = [];
                var promedioSalarioBruto = parseFloat(GananaciaService.getPromedioSalarioBruto()).toFixed(2);
                for (var i = 0; i < vm.listaGanancias.length; i++) {
                    vm.labelsChart.push(vm.listaGanancias[i].noUsuario);
                    var valorNeto = parseFloat(vm.listaGanancias[i].ganancias.totales.valor_neto_periodo).toFixed(2);
                    arregloA.push(valorNeto);
                    arregloB.push(promedioSalarioBruto);
                }

                vm.dataChart.push(arregloA);
                vm.dataChart.push(arregloB);

            }
            function consultarInformeGananciasError(error) {
                console.log(error);
            }
        }

        function generarGraficoPie() {
            vm.dataPie = [];
            vm.labelsPie = [];
            vm.mostrarPieChart();
            vm.consultarInformeGanancias()
                .then(consultarInformeGananciasDone)
                .catch(consultarInformeGananciasError);

            function consultarInformeGananciasDone(results) {
                for (var i = 0; i < vm.listaGanancias.length; i++) {
                    vm.labelsPie.push(vm.listaGanancias[i].noUsuario);
                    var valorNeto = parseFloat(vm.listaGanancias[i].ganancias.totales.valor_neto_periodo).toFixed(2);
                    vm.dataPie.push(valorNeto);
                }
            }
            function consultarInformeGananciasError(error) {
                console.log(error);
            }
        }

        function generarInformeGanancias() {
            vm.mostrarTable();
            vm.consultarInformeGanancias();
        }

        function consultarInformeGanancias() {
            vm.cargandoInforme = true;
            var nuevaListaConsultores = vm.prepararListaConsultores();
            return GananaciaService.prepararLoadListaGanancias(nuevaListaConsultores, vm.fromYear, vm.fromMonth,
                vm.toYear, vm.toMonth)
                .then(loadListaGananciasDone)
                .catch(loadListaGananciasError);

            function loadListaGananciasDone(listaGanancias) {
                console.log("Listo")
                vm.cargandoInforme = false;
                vm.listaGanancias = GananaciaService.getListaGanancias();
                console.log(vm.listaGanancias);
                return listaGanancias;
            }

            function loadListaGananciasError(error) {
                console.log("Error");
                console.log(error);
                vm.cargandoInforme = false;
            }
        }

        function addCheck(even, item) {
            item.checked = true;
        }

        function generateObjeto(objeto) {
            var nuevoObjeto = {
                coUsuario: objeto.co_usuario,
                noUsuario: objeto.no_usuario,
                checked: false
            };
            return nuevoObjeto
        }

        function removeCheck(event, item) {
            item.checked = false;
        }


        function prepararListaConsultores() {
            var newListaConsultores = [];

            for (var i = 0; i < vm.listaConsultores.length; i++) {

                var objeto = vm.listaConsultores[i];

                if (objeto.checked == true) {
                    var nuevoObjeto = {
                        coUsuario: objeto.coUsuario,
                        noUsuario: objeto.noUsuario
                    }
                    newListaConsultores.push(nuevoObjeto);
                }

            }

            return newListaConsultores;
        }



    }
})();
