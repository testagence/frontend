(function () {
    'use strict';
    angular
        .module('app.root', ['app.core']);
    angular
        .module('app.root')
        .controller('RootController', RootController);


    RootController.$inject = ['$state', '$timeout', '$mdSidenav', '$log', '$mdDialog'];
    function RootController($state, $timeout, $mdSidenav, $log, $mdDialog) {
        var vm = this;
        vm.autenticado = true;

        vm.goUrl = goUrl;
        vm.closeLeft = closeLeft;
        vm.openLeft = openLeft;
        vm.closeRight = closeRight;

        vm.datosMenu = [
            { text: 'MENU_AGENCIA', icon: 'business', link: '' },
            { text: 'MENU_PROYECTOS', icon: 'today', link: '' },
            { text: 'MENU_ADMINISTRATIVO', icon: 'supervisor_account', link: '' },
            { text: 'MENU_COMERCIAL', icon: 'card_travel', link: '' },
            { text: 'MENU_FINANCIERO', icon: 'trending_up', link: '' },
            { text: 'MENU_USUARIO', icon: 'people', link: '' },
            { text: 'MENU_LOGOUT', icon: 'exit_to_app', link: '' }
        ];

        vm.goUrl = goUrl;
        vm.isOpenRight = isOpenRight;

        vm.usuario = {};

        vm.toggleLeft = buildDelayedToggler('left');
        vm.toggleRight = buildToggler('right');

        function goUrl(url) {
            vm.toggleLeft();
            $state.go(url);

        }

        function isOpenRight() {
            return $mdSidenav('right').isOpen();
        }



        function debounce(func, wait, context) {
            var timer;

            return function debounced() {
                var context = vm,
                    args = Array.prototype.slice.call(arguments);
                $timeout.cancel(timer);
                timer = $timeout(function () {
                    timer = undefined;
                    func.apply(context, args);
                }, wait || 10);
            };
        }


        function buildDelayedToggler(navID) {
            return debounce(function () {
                // Component lookup should always be available since we are not using `ng-if`
                $mdSidenav(navID)
                    .toggle()
                    .then(function () {
                        $log.debug("toggle " + navID + " is done");
                    });
            }, 200);
        }

        function buildToggler(navID) {
            return function () {
                // Component lookup should always be available since we are not using `ng-if`
                $mdSidenav(navID)
                    .toggle()
                    .then(function () {
                        $log.debug("toggle " + navID + " is done");
                    });
            };
        }


        function closeLeft() {
            $mdSidenav('left').close()
                .then(function () {
                    $log.debug("close LEFT is done");
                });
        }

        function openLeft() {
            $mdSidenav('left').open()
                .then(function () {
                    $log.debug("open LEFT is done");
                });
        }

        function closeRight() {
            $mdSidenav('right').close()
                .then(function () {
                    $log.debug("close RIGHT is done");
                });
        }


    }

})();




