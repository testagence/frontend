(function () {
    'use strict';
    angular
        .module('app.core')
        .config(configuracion)

    configuracion.$inject = ['$stateProvider', '$locationProvider', '$mdThemingProvider', '$httpProvider', '$qProvider', 'miConfiguracion', '$compileProvider', '$mdDateLocaleProvider'];
    function configuracion($stateProvider, $locationProvider, $mdThemingProvider, $httpProvider, $qProvider, miConfiguracion, $compileProvider, $mdDateLocaleProvider) {

        $mdDateLocaleProvider.formatDate = function (date) {
            var m = moment(date);
            return m.isValid() ? m.format('DD-MM-YYYY') : '';
        };
        $compileProvider.preAssignBindingsEnabled(true);
        $qProvider.errorOnUnhandledRejections(false);
        $httpProvider.interceptors.push('HttpInterceptorErrores');


        $mdThemingProvider.theme('default')
            .primaryPalette('blue-grey', { default: '600' })
            .accentPalette('grey', { default: '600' });

        $locationProvider
            .html5Mode({
                enabled: true,
                requireBase: false
            });


        $stateProvider.state(rootState());
        $stateProvider.state(homeState());



        function homeState() {
            return {
                name: 'home',
                url: '/',
                templateUrl: '/app/home/home.html',
                controller: "HomeController",
                component: 'home',
                controllerAs: 'vm',
                parent: 'root'
            }
        }

        function rootState() {
            return {
                name: 'root',  
                abstract: true,
                templateUrl: '/app/layout/root.html',
                controller: "RootController",
                component: 'root',
                controllerAs: 'vm'
            }
        }



    }

})();
