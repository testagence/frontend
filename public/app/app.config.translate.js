(function () {
  'use strict';
  angular
    .module('app.core')
    .config(config);

  config.$inject = ['$translateProvider'];
  function config($translateProvider) {
    $translateProvider.translations('en', {
      APP_NAME: 'Agence',

      FOO: 'This is a paragraph.',
      BUTTON_LANG_EN: 'english',
      BUTTON_LANG_ES: 'spanish',
      BUTTON_YES: 'Yes',
      BUTTON_NOT: 'Not',
      BUTTON_CANCEL: 'Cancel',
      BUTTON_ACCEPT: 'Accept',
      BUTTON_SAVE: 'Save',
      BUTTON_OPTIONS: 'Options',

      MSG_ERR_001: 'Connection error, please try again',
      MSG_ERR_002: 'Error sending data, please verify',
      MSG_ERR_003: 'Internal Error',
      MSG_ERR_004: 'Resource not found',
      MSG_ERR_005: 'Access Forbidden',

      MSG_VAL_001: 'The field is required',
      MSG_VAL_002: 'Should be type a email field valid',

      MSG_ERR_AUTH_001: 'Your are not auteticate',
      MSG_ERR_AUTH_002: 'Your must autenticate again',

      /* DIALOGO DE CONFIRMACIÓN*/
      ALERT_MSG_TITLE: 'Confirm...',
      ALERT_MSG_OK: 'Yes',
      ALERT_MSG_CANCEL: 'No',
      ALERT_MSG_CONFIRM_CREATE: '¿Do you confirm create register?',
      ALERT_MSG_CONFIRM_EDIT: '¿Do you confirm edit register?',




    });
    $translateProvider.translations('es', {
      APP_NAME: 'Agence',
      FOO: 'Este es un párrafo.',
      BUTTON_LANG_EN: 'inglés',
      BUTTON_LANG_ES: 'español',
      BUTTON_YES: 'Si',
      BUTTON_NOT: 'No',
      BUTTON_CANCEL: 'Cancelar',
      BUTTON_ACCEPT: 'Aceptar',
      BUTTON_SAVE: 'Guardar',
      BUTTON_OPTIONS: 'Opciones',



      MSG_ERR_001: 'Error de conexion, por favor intente nuevamente',
      MSG_ERR_002: 'Error al enviar la información, por favor verifique',
      MSG_ERR_003: 'Error interno en servidor',
      MSG_ERR_004: 'Recurso no encontrado',
      MSG_ERR_005: 'Acceso no autorizado',

      MSG_ERR_AUTH_001: 'No se encuentra autenticado',
      MSG_ERR_AUTH_002: 'Lo siento de autenticarse nuevamente',

      MSG_VAL_001: 'El campo es requerido',
      MSG_VAL_002: 'Debe ingresar un Correo Electrónico valido',


      /* DIALOGO DE CONFIRMACIÓN*/
      ALERT_MSG_TITLE: 'Confirmando...',
      ALERT_MSG_OK: 'Si',
      ALERT_MSG_CANCEL: 'No',
      ALERT_MSG_CONFIRM_CREATE: '¿Confirma crear registro?',
      ALERT_MSG_CONFIRM_EDIT: '¿Confirma editar registro?',



      MES_ABRV_01: "Ene",
      MES_ABRV_02: "Feb",
      MES_ABRV_03: "Mar",
      MES_ABRV_04: "Abr",
      MES_ABRV_05: "May",
      MES_ABRV_06: "Jun",
      MES_ABRV_07: "Jul",
      MES_ABRV_08: "Ago",
      MES_ABRV_09: "Sep",
      MES_ABRV_10: "Oct",
      MES_ABRV_11: "Nov",
      MES_ABRV_12: "Dic",

      MES_01: "Enero",
      MES_02: "Febrero",
      MES_03: "Marzo",
      MES_04: "Abril",
      MES_05: "Mayo",
      MES_06: "Junio",
      MES_07: "Julio",
      MES_08: "Agosto",
      MES_09: "Septiembre",
      MES_10: "Octubre",
      MES_11: "Noviembre",
      MES_12: "Diciembre",

      MENU_TITLE:"Menú",

      MENU_AGENCIA: "Agencia",
      MENU_ADMINISTRATIVO: "Administrativo",
      MENU_PROYECTOS: "Proyectos",
      MENU_USUARIO: "Usuarios",
      MENU_COMERCIAL: "Comercial",
      MENU_FINANCIERO: "Financiero",
      MENU_LOGOUT: "Salir",

      TABS_TAB_POR_CONSULTORES: "Por Consultores",
      TABS_TAB_POR_CLIENTES: "Por Clientes",


      TABLA_COL_GANANCIA_PERIODO: "Periodo",
      TABLA_COL_GANANCIA_GANANCIA: "Ganancia Neta",
      TABLA_COL_GANANCIA_COSTO: "Costo Fijo",
      TABLA_COL_GANANCIA_COMISION: "Comisión",
      TABLA_COL_GANANCIA_BENEFICIO: "Beneficio",


      MOD_001_LAB_PERIODO: 'Período',
      MOD_001_LAB_HASTA: "Hasta",
      MOD_001_LAB_MES: 'Mes',
      MOD_001_LAB_YEAR: 'Año',
      MOD_001_LAB_CONSULTORES_DISPONIBLES: "Consultores Disponibles",
      MOD_001_LAB_CONSULTORES_SELECCIONADOS: "Consultores Seleccionados",
      MOD_001_LAB_BTN_RESULTADOS: "Resultados",
      MOD_001_LAB_BTN_GRAFICO: "Grafico",
      MOD_001_LAB_BTN_PIZZA: "Pizza",

      CHART_GANANCIAS:"Ganancia Neta",
      CHART_COSTOS:"Costo Fijo",








    });
    $translateProvider.preferredLanguage('es');
  }

})();
