(function () {
    'use strict';
    angular
        .module('app.core')
        .factory('ConsultoresService', ConsultoresService);


    ConsultoresService.$inject = ['DataService', '$window', '$location', 'miConfiguracion', '$rootScope', '$q'];

    function ConsultoresService(DataService, $window, $location, miConfiguracion, $rootScope, $q) {

        var listaConsultores = [];

        return {
            getListaConsultores: getListaConsultores,
            loadListaConsultores: loadListaConsultores
        }


        function getListaConsultores() {
            return listaConsultores;
        }

        function loadListaConsultores() {
            var url = "consultores";
            var me = this;

            return DataService.get(url)
                .then(loadListaConsultoresDone)
                .catch(loadListaConsultoresError);

            function loadListaConsultoresDone(results) {
                if (results.data.status == true) {
                    me.listaConsultores = results.data.result;
                }
                return results.data;


            }

            function loadListaConsultoresError(error) {
                console.log("Error al cargar data");
                return error;
            }
        }

    }

})();