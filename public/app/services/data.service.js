(function() {
    'use strict';
    angular
        .module('app.core')
        .factory('DataService', DataService );

DataService.$inject = ['$http','$window','$state','miConfiguracion','$mdToast','$filter'];
function DataService($http,$window,$state,miConfiguracion,$mdToast,$filter){

    var $translate = $filter('translate');

    var urlBase= miConfiguracion.pathApi;
    var service={
        get:get,
        post:post,
        put:put
    }

    return service;

    function get (urlPartial){
        var url= urlBase + urlPartial;
        return $http.get(url)
                    .then(handleSuccessGeneral)
                    .catch(handleErrorGeneral);
    }

    function post(urlPartial,datos){
        var url= urlBase + urlPartial;
        return $http.post(url,datos)
                    .then(handleSuccessGeneral)
                    .catch(handleErrorGeneral);
    }

    function put(urlPartial,datos){
        var url= urlBase + urlPartial;
        return $http.put(url,datos)
                    .then(handleSuccessGeneral)
                    .catch(handleErrorGeneral);
    }


    function handleSuccessGeneral(response){
        return response;
    }
    function handleErrorGeneral(error){

        if(error.status==-1){
            $mdToast.showSimple($translate('MSG_ERR_001'));
        }
        
        if(error.status==401){ //NO AUTENTICADO
             try{
                $mdToast.showSimple($translate(error.data.message));
            }catch (err){
                return error;
            }

            $state.go('home');
        }


        if(error.status==403){ //ACCESO A RECURSO PROHIBIDO
             try{
                $mdToast.showSimple($translate(error.data.message));
            }catch (err){
                 return error;
            }
        }

        
        return error;

    }

}
})();
