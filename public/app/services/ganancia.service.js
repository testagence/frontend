(function () {
    'use strict';
    angular
        .module('app.core')
        .factory('GananaciaService', GananaciaService);


    GananaciaService.$inject = ['DataService', '$window', '$location', 'miConfiguracion', '$rootScope', '$q'];

    function GananaciaService(DataService, $window, $location, miConfiguracion, $rootScope, $q) {
        var listaGananciasPorConsultor = [];
        var promedioSalarioBruto=0.0;
        var promedioGananciaNeta=0.0;

        return {
            getListaGanancias: getListaGanancias,
            getPromedioSalarioBruto:getPromedioSalarioBruto,
            prepararLoadListaGanancias: prepararLoadListaGanancias
        }

        function getPromedioSalarioBruto(){
            return promedioSalarioBruto;
        }

        function getListaGanancias() {
            return listaGananciasPorConsultor;
        }

        function calcularTotales() {
            var sumTotalSalarioBruto=0;
            var sumTotalGananciaNeta=0;
            
            for (var i = 0; i < listaGananciasPorConsultor.length; i++) {
                var itemConsultor = listaGananciasPorConsultor[i];

                sumTotalSalarioBruto= sumTotalSalarioBruto + itemConsultor.ganancias.brut_salario;

                var sumCostoFijo = 0;
                var sumGananciaNeta = 0;
                var sumComision = 0;
                var sumBeneficio = 0;

                for (var j = 0; j < itemConsultor.ganancias.resultados_by_periodos.length; j++) {
                    var itemResultados = itemConsultor.ganancias.resultados_by_periodos[j];
                    sumCostoFijo = sumCostoFijo + itemResultados.brut_salario;
                    sumGananciaNeta = sumGananciaNeta + itemResultados.valor_neto_periodo;
                    sumComision = sumComision + itemResultados.comision_periodo;
                    sumBeneficio = sumBeneficio + itemResultados.lucro;
                }

                sumTotalGananciaNeta= sumTotalGananciaNeta + sumGananciaNeta;

                

                listaGananciasPorConsultor[i].ganancias.totales = {
                    valor_neto_periodo: sumGananciaNeta,
                    brut_salario: sumCostoFijo,
                    comision_periodo: sumComision,
                    lucro: sumBeneficio
                }


            }

            promedioSalarioBruto= sumTotalSalarioBruto / listaGananciasPorConsultor.length;
            promedioGananciaNeta= sumTotalGananciaNeta / listaGananciasPorConsultor.length;
        }

        function prepararLoadListaGanancias(listaconsultores, fromYear, fromMonth, toYear, toMonth) {
            listaGananciasPorConsultor = [];

            return loadListaGanacias(listaconsultores, fromYear,
                fromMonth, toYear, toMonth)
                .then(loadListaGanaciasDone);

            function loadListaGanaciasDone(listaGanancias) {
                return listaGanancias;
            }
        }

        function loadListaGanacias(listaconsultores, fromYear, fromMonth, toYear, toMonth) {

            if (listaconsultores.length > 0) {
                var consultor = listaconsultores.pop();
            } else {
                return $q(Promesa);
                function Promesa(resolve, reject) {
                    listaGananciasPorConsultor = listaGananciasPorConsultor.reverse();
                    calcularTotales();
                    return resolve(listaGananciasPorConsultor);
                }
            }

            return loadGanaciaPorConsultor(consultor, fromYear, fromMonth, toYear, toMonth)
                .then(loadGanaciaPorConsultorDone)
                .catch(loadGanaciaPorConsultorError);

            function loadGanaciaPorConsultorDone(gananciaConsultor) {
                consultor.ganancias = gananciaConsultor;
                listaGananciasPorConsultor.push(consultor);
                return loadListaGanacias(listaconsultores, fromYear, fromMonth, toYear, toMonth);
            }

            function loadGanaciaPorConsultorError(error) {
                console.log("Error al obtener ganancias de consultor");
                listaGananciasPorConsultor.push(consultor);
                return loadListaGanacias(listaconsultores, fromYear, fromMonth, toYear, toMonth);

            }


        }

        function loadGanaciaPorConsultor(consultor, fromYear, fromMonth, toYear, toMonth) {
            var url = "ganancia/" + consultor.coUsuario + "/" + fromYear + "/" + fromMonth +
                "/" + toYear + "/" + toMonth;
            return DataService.get(url)
                .then(loadGananciaPorConsultorDone)
                .catch(loadGanaciaPorConsultorError);

            function loadGananciaPorConsultorDone(response) {
                return response.data;
            }

            function loadGanaciaPorConsultorError(error) {
                console.log("Error al intentar obtener datos de consultor");
            }

        }
    }

})();